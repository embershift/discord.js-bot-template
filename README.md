# Discord.js Bot Template

For anyone new or for someone who's lookin' for a template to use every time they start a new bot project.

Edit `package.json` to your liking!

### Credits

Please give credit to 'EmberShift' when using this template. Doesn't have to be everywhere, at least mention us in your help command or something!