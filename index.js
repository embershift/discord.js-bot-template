require('dotenv').config();
const Discord = require('discord.js');
const bot = new Discord.Client();
const fs = require('fs');
const config = require('./json/config.json');
const errors = require('./json/errors.json');
const emojis = require('./json/emojis.json');

bot.prefix = config.prefix;
bot.specialPerms = config.specialPerms;
bot.handlers = ["cmd", "evn"];
bot.cmdModules = ["main"]
bot.commands = new Discord.Collection();
bot.aliases = new Discord.Collection();
bot.descriptions = new Discord.Collection();
bot.usages = new Discord.Collection();

bot.handlers.forEach(handler => {
	require(`./handlers/${handler}`)(bot)
})

bot.login(process.env.TOKEN);