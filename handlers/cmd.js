const fs = require('fs');

module.exports = (bot) => {
	try {
		bot.cmdModules.forEach((m) => {
			fs.readdir(`./commands/${m}`, (err, files) => {
				let r = files.filter(f => f.split('.').pop() === 'js');
				r.forEach(e => {
					const f = require(`./commands/${e}`);
					bot.commands.set(f.data.name, f);
					if(f.data.aliases && Array.isArray(f.data.aliases)) 
						f.data.aliases.forEach(alias => {bot.aliases.set(alias, f.data.name)});
					bot.descriptions.set(f.data.desc, f.data.name);
					bot.usages.set(f.data.usage, f.data.name);
					console.log(`[COMMAND-LOADED] ${f.data.name}`)
				})
			})
		})
	} catch (err) {
		if (err) throw err;
	}
}