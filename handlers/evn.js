const fs = require('fs');
module.exports = async (bot) => {
	try {
		const loadEvent = (directory) => {
			try {
				fs.readdir(`../events/${directory}`, (err, files) => {
					if (err) throw err;
					let n = files.filter(f => f.split('.').pop() === 'js');
					
					n.forEach(e => {
						const event = require(`../events/${directory}/${e}`)(bot);
						bot.on(event.name, event.execute())
					})
				})
			} catch (err) {throw err}
		}
	}catch (err) {throw err}
}